const assert = require('assert');
const User = require('../src/user');

describe('Validation records', function () {

    it('requires a user name', () => {
        const undefinedUser = new User({name: undefined});
        const validationResult = undefinedUser.validateSync();
        console.log(validationResult);
        assert(validationResult.errors.name.message === 'Name is required.');
    });

    it('requires a name longer that 2 characters', () => {
        const shortUser = new User({name: 'JW'});
        const validationResult = shortUser.validateSync();
        assert(validationResult.errors.name.message === 'Name must be longer than 2 characters.');
    });

    it('does not save invalid data', (done) => {
        const undefinedUser = new User({name: undefined});
        undefinedUser.save()
            .catch((validationResult) => {
                const {message} = validationResult.errors.name;
                assert(validationResult.errors.name.message === 'Name is required.');
                done();
            })
    })
});
