const assert = require('assert');
const User = require('../src/user');

describe('Deleting a user', () => {

    let james;

    beforeEach((done) => {
        james = new User({name: 'James'});
        james.save().then(() => done());
    });

    it('model instance remove', (done) => {
        james.remove()
            .then(() => User.findOne({name: 'James'}))
            .then(user => {
                console.log(user);
                assert(user === null);
                done();
            })
    });

    it('class method remove', (done) => {
        User.remove({name: 'James'})
            .then(() => User.findOne({name: 'James'}))
            .then(user => {
                assert(user === null);
                done();
            });
    });

    it('class method findOneAndRemove', (done) => {
        User.findOneAndRemove({name: 'James'})
            .then(() => User.findOne({name: 'James'}))
            .then(user => {
                assert(user === null);
                done();
            });
    });

    it('class method findByIdAndRemove', (done) => {
        User.findByIdAndRemove(james.id)
            .then(() => User.findOne({name: 'James'}))
            .then(user => {
                assert(user === null);
                done();
            });
    })
});
