const assert = require('assert');
const User = require('../src/user');

describe('Reading users out of the database', () => {

    let james;

    beforeEach((done) => {
        james = new User({name: 'James'});
        james.save()
            .then(() => done());
    });

    it('finds the user with the name James', (done) => {
        User.findOne({name: 'James'})
            .then((user) => {
                assert(user._id.toString() === james._id.toString());
                done();
            });
    });

    it('finds the user with an id', (done) => {
        User.findOne({_id: james._id})
            .then(user => {
                assert(user.name === james.name);
                done();
            });
    });
});
