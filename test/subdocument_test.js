const assert = require('assert');
const User = require('../src/user');

describe('Subdocuments', () => {

    it('can create a subdocument', done => {
        const james = new User({
            name: 'James',
            posts: [{title: 'My post'}]
        });
        james.save()
            .then(() => User.findOne({name: 'James'}))
            .then(user => {
                assert(user.posts[0].title === 'My post');
                done();
            })
    });

    it('can add subdocuments to existing record', done => {
        const james = new User({name: 'James', posts: []});
        james.save()
            .then(() => User.findOne({name: 'James'}))
            .then(user => {
                assert(user.name === 'James');
                user.posts.push({title: 'New post'});
                user.save()
                    .then(() => User.findById(james._id))
                    .then(user => {
                        console.log(user);
                        assert(user.posts[0].title === 'New post');
                        done();
                    });
            })
    });

    it('can remove an existing subdocument', done => {
        const james = new User({name: 'James', posts: [{title: 'Post to delete'}]});
        james.save()
            .then(() => User.findOne({name: 'James'}))
            .then(user => {
                const post = user.posts[0];
                post.remove();
                return user.save();
            })
            .then(() => User.findOne({name: 'James'}))
            .then(user => {
                assert(user.posts.length === 0);
                done();
            });
    })
});
