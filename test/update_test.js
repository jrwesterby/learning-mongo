const assert = require('assert');
const User = require('../src/user');

describe('Updating a user', () => {

    let james;

    beforeEach((done) => {
        james = new User({name: 'James', postCount: 0});
        james.save().then(() => done());
    });

    function assertName(name, operation) {
        return operation
            .then(() => User.find({}))
            .then(users => {
                assert(users.length === 1);
                assert(users[0].name === name);
            });
    }

    it('instance type using set and save', (done) => {
        james.set('name', 'John');
        assertName('John', james.save()).then(() => done());
    });

    it('a model instance can update', (done) => {
        assertName('Robert', james.update({name: 'Robert'})).then(() => done());
    });

    it('a model class can update', (done) => {
        assertName('Robert', User.update({name: 'James'}, {name: 'Robert'})).then(() => done());
    });

    it('a model class can update by id', (done) => {
        assertName('Robert', User.findByIdAndUpdate(james._id, {name: 'Robert'})).then(() => done());
    });

    it('a users post count can be incremented by 1', (done) => {
        User.update({name: 'James'}, {$inc: {postCount: 1}})
            .then(() => User.findOne({name: 'James'})
                .then(user => {
                    assert(user.postCount === 1);
                    done();
                })
            );
    });
});
